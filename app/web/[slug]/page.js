import { getArticles, getArticleBySlug } from '@/lib/newt'
import '@/app/module.scss'
import styles from '@/app/page.module.css'


export async function generateStaticParams() {
  const articles = await getArticles()
  return articles.map((article) => ({
    slug: article.slug,
  }))
}
export const dynamicParams = false

export async function generateMetadata({ params }) {
  const { slug } = params
  const article = await getArticleBySlug(slug)

  return {
    title: `${article?.title} | suzuseed` ,
    description: `${article?.title}のページです。`,
  }
}

export default async function Article({ params }) {
  const { slug } = params
  const article = await getArticleBySlug(slug)
  if (!article) return

  return (
    <main className={styles.main}>
      <h1>{article.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: article.body }} />
      <div className={styles.coverImage}>
        <img src={article.coverImage.src} alt={article.coverImage.altText} />
      </div>


      <section className={styles.section}>
        <h2>使用技術</h2>
        <ul className={styles.tag}>
          {
            article.tags.map((tag) => {
              return <li key={tag}>{tag.name}</li>
            })
          }
        </ul>
      </section>
    </main>
  )
}