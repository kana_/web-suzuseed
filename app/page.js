import Link from 'next/link'
import { getArticles } from '@/lib/newt'
import styles from '@/app/page.module.css'
import '@/app/module.scss'

export const metadata = {
  title: '制作したコンテンンツ一覧 | suzuseed',
  description: 'suzuseedが制作したコンテンンツの一覧ページです',
}

export default async function Home() {
  const articles = await getArticles()
  return (
    <main className={styles.main}>
      <h1>制作したコンテンンツ一覧</h1>
      <ul>
        {articles.map((article) => {
          return (
            <li key={article._id}>
              <Link href={`web/${article.slug}`}>{article.title}</Link>
            </li>
          )
        })}
      </ul>
    </main>
  )
}